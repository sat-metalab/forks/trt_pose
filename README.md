# trt_pose

> Want to detect hand poses?  Check out the new [trt_pose_hand](http://github.com/NVIDIA-AI-IOT/trt_pose_hand) project for real-time hand pose and gesture recognition!

<img src="https://user-images.githubusercontent.com/4212806/67125332-71a64580-f1a9-11e9-8ee1-e759a38de215.gif" height=256/>

trt_pose is aimed at enabling real-time pose estimation on NVIDIA Jetson.  You may find it useful for other NVIDIA platforms as well.  Currently the project includes

- Pre-trained models for human pose estimation capable of running in real time on Jetson Nano.  This makes it easy to detect features like ``left_eye``, ``left_elbow``, ``right_ankle``, etc.

- Training scripts to train on any keypoint task data in [MSCOCO](https://cocodataset.org/#home) format.  This means you can experiment with training trt_pose for keypoint detection tasks other than human pose.

To get started, follow the instructions below.  If you run into any issues please [let us know](../../issues).

## Getting Started

To get started with trt_pose, follow these steps.

### Step 1 - Install Dependencies

1. Install PyTorch and Torchvision and TensorRT 
   * NVIDIA Jetson (following [this guide](https://forums.developer.nvidia.com/t/72048), tested with Xavier NX running JetPack 4.6 L4T 32.6.1):
      * PyTorch
        ```
        wget https://nvidia.box.com/shared/static/fjtbno0vpo676a25cgvuqc1wty0fkkg6.whl -O torch-1.10.0-cp36-cp36m-linux_aarch64.whl
        sudo apt-get install python3-pip libopenblas-base libopenmpi-dev
        pip3 install Cython numpy!=1.19.5 # avoid this version otherwise torch2trt installation exits with: Illegal instruction
        pip3 install torch-1.10.0-cp36-cp36m-linux_aarch64.whl
        ```
      * Torchvision
        ```
        git clone --branch release/0.11 https://github.com/pytorch/vision torchvision
        cd torchvision
        python3 setup.py install --user
        ```
      * TensorRT
        ```
        sudo apt install python3-libnvinfer
        ```
   * other NVIDIA GPUs supporting CUDA 11.x (following [this guide](https://pytorch.org/get-started/locally/), tested with Ubuntu 20.04 using [CUDA 11.x from NVIDIA repos](https://docs.nvidia.com/deeplearning/cudnn/install-guide/index.html#package-manager-ubuntu-install)):
     ```
     pip3 install Cython numpy
     sudo pip3 install torch==1.10.0+cu113 torchvision==0.11.1+cu113 -f https://download.pytorch.org/whl/cu113/torch_stable.html
     sudo apt install python3-libnvinfer
     ```

2. Install [torch2trt](https://github.com/NVIDIA-AI-IOT/torch2trt)
   * TensorRT 8+ and JetPack 4.6 require for now a fork of torch2rt (until https://github.com/NVIDIA-AI-IOT/torch2trt/pull/656 or equivalent is merged):

    ```python
    git clone https://github.com/chaoz-dev/torch2trt -b chaoz-dev/chitoku/jp4.6_tensorrt8
    cd torch2trt
    ```
   * NVIDIA Jetson (tested with Xavier NX running JetPack 4.6 L4T 32.6.1):
     ```
     sudo apt install cuda-nvcc-10-2 cuda-nvtx-10-2 libcublas-dev libcusolver-dev-10-2 libcusparse-dev-10-2 libnvinfer-dev ninja-build
     sudo \
        CUDA_HOME=/usr/local/cuda-10.2 \
        python3 setup.py install --plugins
     ```
   * other NVIDIA GPUs supporting CUDA 11.x (tested with Ubuntu 20.04 using [CUDA 11.x from NVIDIA repos](https://docs.nvidia.com/deeplearning/cudnn/install-guide/index.html#package-manager-ubuntu-install)):
     ```
     sudo apt install cuda-cudart-dev-11-4 cuda-nvcc-11-4 libcublas-dev-11-4 libcusolver-dev-11-4 libcusparse-dev-11-4 libnvinfer-dev ninja-build
     sudo \
        CUDA_HOME=/usr/local/cuda-11.4 \
        CPATH=/usr/local/cuda-11.4/targets/x86_64-linux/include:$CPATH \
        LD_LIBRARY_PATH=/usr/local/cuda-11.4/targets/x86_64-linux/lib/:$LD_LIBRARY_PATH \
        python3 setup.py install --plugins
     ```

3. Install JetCam

    ```python
    git clone https://github.com/NVIDIA-AI-IOT/jetcam
    cd jetcam
    sudo python3 setup.py install
    ```

4. Install other miscellaneous packages

    ```python
    sudo apt-get install ipython3 jupyter python3-matplotlib python3-nbformat python3-tqdm 
    sudo pip3 install ipywidgets pycocotools 
    sudo jupyter nbextension enable --py --sys-prefix widgetsnbextension
    ```
    
### Step 2 - Install trt_pose

```python
git clone https://gitlab.com/sat-metalab/forks/trt_pose
cd trt_pose
sudo python3 setup.py install
```
### Step 3 - Run the example notebook

We provide a couple of human pose estimation models pre-trained on the MSCOCO dataset.  The throughput in FPS is shown for each platform

| Model | Jetson Nano | Jetson Xavier | Weights |
|-------|-------------|---------------|---------|
| resnet18_baseline_att_224x224_A | 22 | 251 | [download (81MB)](https://drive.google.com/open?id=1XYDdCUdiF2xxx4rznmLb62SdOUZuoNbd) |
| densenet121_baseline_att_256x256_B | 12 | 101 | [download (84MB)](https://drive.google.com/open?id=13FkJkx7evQ1WwP54UmdiDXWyFMY1OxDU) |

To run the live Jupyter Notebook demo on real-time camera input, follow these steps
 
1. Download the model weights using the link in the above table into the [tasks/human_pose](tasks/human_pose) directory
    ```
    wget --no-check-certificate 'https://docs.google.com/uc?export=download&id=1XYDdCUdiF2xxx4rznmLb62SdOUZuoNbd' -O tasks/human_pose/resnet18_baseline_att_224x224_A_epoch_249.pth
    wget --no-check-certificate 'https://docs.google.com/uc?export=download&id=13FkJkx7evQ1WwP54UmdiDXWyFMY1OxDU' -O tasks/human_pose/densenet121_baseline_att_256x256_B.pth
    ```
2. Open and follow the [live_demo.ipynb](tasks/human_pose/live_demo.ipynb) notebook

    > You may need to modify the notebook, depending on which model you use
    ```
    cd tasks/human_pose
    ```
    * to run the notebook interactively from your web browser (the URL will appear on the terminal):
        ```
        ip=`hostname -I`
        jupyter-notebook --ip=$ip --no-browser live_demo.ipynb
        ```
    * to run the notebook from the commandline without interaction:
        ```
        jupyter nbconvert --execute --to notebook --ExecutePreprocessor.timeout=-1 live_demo.ipynb
        ```
    * to convert the notebook to a python script:
        ```
        jupyter nbconvert --to python live_demo.ipynb
        ```

## See also

- [trt_pose_hand](http://github.com/NVIDIA-AI-IOT/trt_pose_hand) - Real-time hand pose estimation based on trt_pose
- [torch2trt](http://github.com/NVIDIA-AI-IOT/torch2trt) - An easy to use PyTorch to TensorRT converter

- [JetBot](http://github.com/NVIDIA-AI-IOT/jetbot) - An educational AI robot based on NVIDIA Jetson Nano
- [JetRacer](http://github.com/NVIDIA-AI-IOT/jetracer) - An educational AI racecar using NVIDIA Jetson Nano
- [JetCam](http://github.com/NVIDIA-AI-IOT/jetcam) - An easy to use Python camera interface for NVIDIA Jetson

## References

The trt_pose model architectures listed above are inspired by the following works, but are not a direct replica.  Please review the open-source code and configuration files in this repository for architecture details.  If you have any questions feel free to reach out.

*  _Cao, Zhe, et al. "Realtime multi-person 2d pose estimation using part affinity fields." Proceedings of the IEEE Conference on Computer Vision and Pattern Recognition. 2017._

*  _Xiao, Bin, Haiping Wu, and Yichen Wei. "Simple baselines for human pose estimation and tracking." Proceedings of the European Conference on Computer Vision (ECCV). 2018._
